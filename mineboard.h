#ifndef MINEBOARD_H
#define MINEBOARD_H

#include <vector>
using namespace std;

enum GridState
{
	Mine = -1,
	Empty = 0,
	Number
};

enum GameState
{
	Win = 0,
	Lose,
	Continue
};

class MineBoard
{
public:
	MineBoard(int boardsize, int minecount);
	~MineBoard();

	void createBoard();//创建随机棋盘
	void pick(int row, int column);//点开未知点
	GameState checkState();//游戏结果
	vector<vector<pair<int, int>>> boardState();//盘面数据状态

private:
	void resetBoard();//重置为0

private:
	//pair.first实际数据 -1表示雷 0表示空 其他数字表示周边雷数
	//pair.second 0未知状态 1已知状态
	vector<vector<pair<int, int>>> data;
	int mines; //雷数
};

#endif // MINEBOARD_H