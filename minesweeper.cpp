#include "minesweeper.h"
#include "ui_minesweeper.h"
#include "mineboard.h"
#include <QMouseEvent>

MineSweeper::MineSweeper(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::MineSweeper)
{
	ui->setupUi(this);
	board = new MineBoard(8, 10);
	QGridLayout *layout = new QGridLayout(ui->widget);
	layout->setSpacing(0);
	layout->setVerticalSpacing(0);
	for (int i = 0; i < 8 * 8; i++)
	{
		auto button = new QPushButton(ui->widget);
		button->setFixedWidth(50);
		button->setFixedHeight(50);
		button->setProperty("index", i);
		button->setProperty("flag", false); //是否有小旗子标志
		button->setStyleSheet("background-color: grey;");
		button->installEventFilter(this); //获取右键点击事件
		gridbuttons.append(button);
		layout->addWidget(button, i / 8, i % 8);
	}
	ui->widget->hide();
}

MineSweeper::~MineSweeper()
{
	delete ui;
}

bool MineSweeper::eventFilter(QObject *obj, QEvent *ev)
{
	if (ev->type() == QEvent::MouseButtonPress)
	{
		QPushButton *button = qobject_cast<QPushButton *>(obj);
		QMouseEvent *event = dynamic_cast<QMouseEvent *>(ev);
		if (event->button() == Qt::RightButton) //右键点击
		{
			bool flag = button->property("flag").toBool();
			button->setProperty("flag", !flag);
			updateUI(board->checkState() == GameState::Lose);
		}
		else if (event->button() == Qt::LeftButton) //左键点击
		{
			QPushButton *button = qobject_cast<QPushButton *>(obj);
			int index = button->property("index").toInt();
			board->pick(index / 8, index % 8);
			GameState result = board->checkState();
			if (result == GameState::Win)
			{
				ui->label->setText("success");
				ui->label->setStyleSheet("background-color: red;font-size:30px");
				ui->pushButton->setText("restart");
			}
			else if (result == GameState::Lose)
			{
				ui->label->setText("failure");
				ui->label->setStyleSheet("background-color: pink;font-size:30px");
				ui->pushButton->setText("restart");
			}
			else
			{
				ui->label->setText("continue");
			}
			updateUI(result == GameState::Lose);
		}
		return true;
	}
	return QWidget::eventFilter(obj, ev);
}

void MineSweeper::on_pushButton_clicked()
{
	ui->widget->show();
	board->createBoard();
	for (auto &button : gridbuttons)
	{
		button->setProperty("flag", false);
	}
	updateUI();
	ui->label->setText("continue");
}

void MineSweeper::updateUI(bool showmine)
{
	auto state = board->boardState();
	int size = state.size();
	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			gridbuttons[i * 8 + j]->setText(""); //清空
			if (state[i][j].second == 1)
			{
				if (state[i][j].first == 0)
				{
					gridbuttons[i * 8 + j]->setStyleSheet("background-color: white;");
				}
				else if (state[i][j].first == -1)
				{
					gridbuttons[i * 8 + j]->setStyleSheet("background-color: red;");
				}
				else
				{
					gridbuttons[i * 8 + j]->setText(QString::number(state[i][j].first));
					gridbuttons[i * 8 + j]->setStyleSheet("background-color: white;font-size:40px");
				}
			}
			else
			{
				if (showmine && state[i][j].first == -1)
				{
					gridbuttons[i * 8 + j]->setStyleSheet("background-color: red;");
				}
				else
				{
					gridbuttons[i * 8 + j]->setStyleSheet("background-color: grey;");
					bool flag = gridbuttons[i * 8 + j]->property("flag").toBool();
					if (flag)
					{
						gridbuttons[i * 8 + j]->setStyleSheet("background-color: blue;");
					}
				}
			}
		}
	}
}