#ifndef MINESWEEPER_H
#define MINESWEEPER_H

#include <QWidget>

namespace Ui
{
class MineSweeper;
}
class QPushButton;
class MineBoard;

class MineSweeper : public QWidget
{
	Q_OBJECT

public:
	explicit MineSweeper(QWidget *parent = 0);
	~MineSweeper();

protected:
	bool eventFilter(QObject *obj, QEvent *ev);

private slots:
	void on_pushButton_clicked();
	void updateUI(bool showmine = false);

private:
	Ui::MineSweeper *ui;
	MineBoard *board;
	QList<QPushButton *> gridbuttons;
};

#endif // MINESWEEPER_H
