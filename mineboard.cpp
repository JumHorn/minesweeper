#include <cstdlib>
#include <ctime>
#include <algorithm>
#include "mineboard.h"

MineBoard::MineBoard(int boardsize, int minecount) : data(boardsize, vector<pair<int, int>>(boardsize))
{
	mines = minecount;
	srand((unsigned int)time(NULL));
}

MineBoard::~MineBoard()
{
}

void MineBoard::createBoard()
{
	resetBoard();
	int size = data.size();
	int count = size * size;
	for (int i = 0; i < mines;)
	{
		int tmp = rand() % count;
		if (data[tmp / size][tmp % size].first == 0)
		{
			data[tmp / size][tmp % size].first = -1;
			++i;
		}
	}

	//create number
	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			if (data[i][j].first == 0)
			{
				int count = 0;
				for (int m = max(0, i - 1); m <= min(size - 1, i + 1); m++)
				{
					for (int n = max(0, j - 1); n <= min(size - 1, j + 1); n++)
					{
						if (data[m][n].first == -1)
							++count;
					}
				}
				data[i][j].first = count;
			}
		}
	}
}

void MineBoard::resetBoard()
{
	for (auto &row : data)
	{
		for (auto &n : row)
		{
			n.first = 0;
			n.second = 0;
		}
	}
}

void MineBoard::pick(int row, int column)
{
	int size = data.size();
	if (row < 0 || row >= size || column < 0 || column >= size)
		return;
	if (data[row][column].second == 1) //已知状态，直接返回
		return;
	data[row][column].second = 1;
	if (data[row][column].first != 0)
		return;
	pick(row - 1, column);
	pick(row + 1, column);
	pick(row, column - 1);
	pick(row, column + 1);
}

GameState MineBoard::checkState()
{
	bool isWin = true;
	for (auto &row : data)
	{
		for (auto &n : row)
		{
			if (n.first == -1 && n.second == 1)
				return GameState::Lose;
			if (n.first != -1 && n.second == 0)
				isWin = false;
		}
	}
	return isWin ? GameState::Win : GameState::Continue;
}

vector<vector<pair<int, int>>> MineBoard::boardState()
{
	return data;
}